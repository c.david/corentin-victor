#ifndef PLAYLIST_H
#define PLAYLIST_H

// Librairiess
#include <vector>
#include "Track.h"

/**
 * \file Playlist.h
 * \brief Represnet an Artist
 * \version 0.0.1
 * \author LECOMTE Victor && DAVID Corentin
 */
class Playlist {

private :
  //Attibutes
  unsigned int id; ///< Playlist identifier
  unsigned int duration; ///< Playlist duration
  std::vector<Track> tracks; ///< List of Track in a Playlist
  
public :
// Default constructor
  /**
   * \brief Default constructor
   */
  Playlist();
// Constructor with parameters
  /**
   * \brief Constructor with parameters
   */
  Playlist(unsigned int, unsigned int, std::vector<Track>);

// Destructive
  /**
   * \brief Destructor
   */
  ~Playlist();

// Methods
  /**
   * \brief Methods for write pth of playlist in M3U file
   */
  void writeM3U();
   /**
    * \brief Methods for write pth of playlist in XSPF file
    */
  void writeXSPF();
  
// Accessors (getters)
    /**
    * \brief Allows the recovery of the id
    */
  unsigned int getId();
   /**
    * \brief Allows the recovery of the duration
    */
  unsigned int getDuration();
   /**
   * \brief Allows the recovery of a Track
   */
  std::vector<Track> getTrack();
  
// Mutators (setters)
  /**
   * \brief Allows the modification of the duration
   */
  void setDuration(unsigned int);
  /**
   * \brief Allows the modification of a Track
   */
  void setTrack(std::vector<Track>);
};

#endif // PLAYLIST_H

  
