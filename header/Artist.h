#ifndef ARTIST_H
#define ARTIST_H

// Librairies
#include <string>
/**
 * \file Artist.h
 * \brief Represent an Artist
 * \version 0.0.1
 * \author LECOMTE Victor && DAVID Corentin
 */
class Artist {
  
private:
// Attributes
  unsigned int id; ///< Artist identifier
  std::string name; ///< Artist name as string

public:
// Default constructor  
  /**
   * \brief Default constructor 
   */
  Artist();

// Constructor with parameters
  /**
   * \brief Constructor wih parameters 
   */
  Artist(unsigned int, std::string);

// Destructive
  /**
   * \brief Destructor
   */
  ~Artist();
  
  
// Accessors (getters)
   /**
   * \brief Allows the recovery of the identifier
   */
  unsigned int getId();

  /**
   * \brief Allows the recovery of the name
   */
  std::string  getName();

// Mutators (setters)

  /**
   * \brief Allows the modification of the name
   */
  void setName(std::string);
};
#endif // ARTIST_H
