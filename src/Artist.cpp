//Librairies
#include "../header/Artist.h"
#include <string>
#include <iostream>

//Default constructor
Artist::Artist() :
  id(1),
  name("Eminem")

{
  std::cout << " " << std::endl;
  std::cout << "Default constructors of the class Album" << std::endl;
  std::cout << "L'id de l'artist est : " << id << std::endl;
  std::cout << "Le nom de l'artist est : " << name << std::endl;
}

//Constructor with parameters
Artist::Artist(unsigned int  _id,
               std::string _name
              ) :
  id(_id),
  name(_name)

{
  std::cout << " " << std::endl;
  std::cout << "Constrctor with parameters" << std::endl;
  std::cout << "L'id de l'artist est : " << id << std::endl;
  std::cout << "Le nom de l'artist est : " << name << std::endl;
}

//Destructive
Artist::~Artist() {}

// Accessors (getters)
unsigned int Artist::getId()
{
  return id;
}

std::string  Artist::getName()
{
  return name;
}

// Mutators (setters)

void Artist::setName(std::string _name)
{
  name =  _name;
}
