//Librairies
#include "../header/Subgenre.h"
#include <string>
#include <iostream>

//Default constructor
Subgenre::Subgenre() :
  id(1),
  sub_type(2)

{
  std::cout << " " << std::endl;
  std::cout << "Le constructeur par défaut est composé de la manière suivante" << std::endl;
  std::cout << "L'id du sous genre est : " << id << std::endl;
  std::cout << "Le sous type est : " << sub_type << std::endl;
}

//Constructor with parameters
Subgenre::Subgenre(unsigned int  _id,
                   short int _sub_type
                  ) :
  id(_id),
  sub_type(_sub_type)

{
  std::cout << " " << std::endl;
  std::cout << "Constructor with parameters" << std::endl;
  std::cout << "L'id du sous genre est : " << id << std::endl;
  std::cout << "Le sous type est : " << sub_type << std::endl;
}

//Destructive
Subgenre::~Subgenre() {}

// Accessors (getters)
unsigned int Subgenre::GetId()
{
  return id;
}

short int Subgenre::getSub_Type()
{
  return sub_type;
}

// Mutators (setters)
void Subgenre::setSubtype(short int _sub_type)
{
  sub_type = _sub_type;
}
