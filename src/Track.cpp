#include <iostream>
#include "../header/Track.h"
#include "../header/Album.h"
#include "../header/Artist.h"

// Default constructor
Track::Track() :
  id(0),
  duration(0),
  name(""),
  path(""),
  theAlbum(),
  theArtist(),
  anAlbum(),
  anArtist(),
  aFormat(),
  aGenre(),
  aSubgenre(),
  aPolyphony()
{
  std::cout << " " << std::endl;
  std::cout << "Default constructors of the class Album" << std::endl;
  std::cout << "L'id du morceau est : " << id << std::endl;
  std::cout << "La durée du morceau est de : " << duration << " seconds" << std::endl;
  std::cout << "Le nom du morceau est : " << name << std::endl;
  std::cout << "Le chemin du morceau est : " << path << std::endl;
 }

// Constructors with parameters
Track::Track(unsigned int _id,
             unsigned int _duration,
             std::string _name,
             std::string _path,
             std::vector<Album> _theAlbum,
             std::vector<Artist> _theArtist,
             Format _aFormat,
             Genre _aGenre,
             Subgenre _aSubgenre,
             Polyphony _aPolyphony
            ) :
  id(_id),
  duration(_duration),
  name(_name),
  path(_path),
  theAlbum(_theAlbum),
  theArtist(_theArtist),
  aFormat(_aFormat),
  aGenre(_aGenre),
  aSubgenre(_aSubgenre),
  aPolyphony(_aPolyphony)
{
  std::cout << " " << std::endl;
  std::cout << "Constructors with  parameters" << std::endl;
  std::cout << "L'i du morceau est : " << id << std::endl;
  std::cout << "La durée du morceau est de : " << duration << " seconds" << std::endl;
  std::cout << "Le nom du morceau est : " << name << std::endl;
  std::cout << "Le chemin du morceau est : " << path << std::endl;
}



Track::Track(unsigned int _id,
             unsigned int _duration,
             std::string _name,
             std::string _path,
             Album _anAlbum,
             Artist _anArtist,
             Format _aFormat,
             Genre _aGenre,
             Subgenre _aSubgenre,
             Polyphony _aPolyphony
            ) :
  id(_id),
  duration(_duration),
  name(_name),
  path(_path),
  anArtist(_anArtist),
  anAlbum(_anAlbum),
  aFormat(_aFormat),
  aGenre(_aGenre),
  aSubgenre(_aSubgenre),
  aPolyphony(_aPolyphony)
{
  std::cout << " " << std::endl;
  std::cout << "Constructors with  parameters" << std::endl;
  std::cout << "L'i du morceau est : " << id << std::endl;
  std::cout << "La durée du morceau est de : " << duration << " seconds" << std::endl;
  std::cout << "Le nom du morceau est : " << name << std::endl;
  std::cout << "Le chemin du morceau est : " << path << std::endl; 
}


// Destructive
Track::~Track() {}

// Accessors (getters)
unsigned int Track::getId()
{
  return id;
}

unsigned int Track::getDuration()
{
  return duration;
}

std::string Track::getName()
{
  return name;
}

std::string Track::getPath()
{
  return path;
}

std::vector<Album> Track::getTheAlbum()
{
  return theAlbum;
}

std::vector<Artist> Track::getTheArtist()
{
  return theArtist;
}

Album Track::getAlbum()
{
  return anAlbum;
}

Artist Track::getArtist()
{
  return anArtist;
}

Format Track::getFormat()
{
  return aFormat;
}

Genre Track::getGenre()
{
  return aGenre;
}

Subgenre Track::getSubgenre()
{
  return aSubgenre;
}

Polyphony Track::getPolyphony()
{
  return aPolyphony;
}




// Mutators (setters)
void Track::setDuration(unsigned int _duration)
{
  duration = _duration;
}

void Track::setName(std::string _name)
{
  name = _name;
}

void Track::setPath(std::string _path)
{
  path = _path;
}

void Track::setTheAlbum(std::vector<Album> _theAlbum)
{
  theAlbum = _theAlbum;
}

void Track::setTheArtist(std::vector<Artist> _theArtist)
{
  theArtist = _theArtist;
}

void Track::setAlbum(Album _anAlbum)
{
  anAlbum = _anAlbum;
}

void Track::setArtist(Artist _anArtist)
{
  anArtist = _anArtist;
}

void Track::setFormat(Format _aFormat)
{
  aFormat = _aFormat;
}

void Track::setGenre(Genre _aGenre)
{
  aGenre = _aGenre;
}

void Track::setSubgenre(Subgenre _aSubgenre)
{
  aSubgenre = _aSubgenre;
}

void Track::setPolyphony(Polyphony _aPolyphony)
{
  aPolyphony = _aPolyphony;
}


