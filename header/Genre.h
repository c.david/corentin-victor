#ifndef GENRE_H
#define GENRE_H

// Librairies
#include <string>

/**
 * \file Genre.h
 * \brief Reprent a Format
 * \version 0.0.1
 * \author LECOMTE Victor && DAVID Corentin
 */
class Genre {
private:
  // Attributes
  unsigned int id; ///< Format identifier
  std::string type; ///< Format type as string

public:
// Default constructor
  /**
   * \brief Default constructor
   */
  Genre();

// Constructor with parameters
  /**
   * \brief Constructor with parameter
   */
  Genre(unsigned int, std::string);

// Destructive
  /**
   * \brief Destructor
   */
  ~Genre();

// Accessors (getters)
  /**
   * \brief Allows the recovery of the id
   */
  unsigned int getId();
  /**
   * \brief Allow the recovery of the type
   */
  std::string getType();

// Mutators (setters)
  /**
   * \brief Allows the modification of the type
   */
  void setType(std::string);
};
#endif // GENRE_H
