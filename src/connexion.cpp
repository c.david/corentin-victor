#include <iostream>
#include <cstring>
#include <string>
#include <libpq-fe.h>
#include <syslog.h>


int main()
{
  openlog(NULL, LOG_CONS | LOG_PID, LOG_USER);
  const char *connexion_infos = "host=postgresql.bts-malraux72.net port=5432 dbname=Cours user=v.lecomte password=P@ssword connect_timeout=5";
  PGPing ping;
  ping = PQping(connexion_infos);

  if(PGPing(ping) == PQPING_OK)
  {
    PGconn *connexion;
    std::cout << "Ping reussi...\nConnexion en cours" << std::endl;
    syslog(LOG_INFO, "Successful ping");
    connexion = PQconnectdb(connexion_infos);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      std::cout << "La connexion au serveur de base de données a été établie avec succès" << std::endl;
      syslog(LOG_INFO, "Success connection to postgreSQL");
      // TODO: #18
      PGresult *resultat = PQexec(connexion, "SELECT chemin FROM \"radio_libre\".\"morceau\";");
      ExecStatusType code_retour_execution = PQresultStatus(resultat);

      if(code_retour_execution == PGRES_TUPLES_OK)
      {
        char *valeur_champ;
        std::cout << "Voici la liste des chemins de tous les morceaux disponibles dans la base de données" << std::endl;

        for(unsigned int compteur_tuple = 0; (int)compteur_tuple < PQntuples(resultat); ++compteur_tuple)
        {
          for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(resultat); ++compteur_champ)
          {
            valeur_champ = PQgetvalue(resultat, compteur_tuple, compteur_champ);
            std::cout << valeur_champ << std::endl;
          }
        }
      }
    }
    else
    {
      std::cout << "Malheuresement le serevur n'est pas joignable. Vérifié la connextivité" << std::endl;
      syslog(LOG_CRIT, "the server is not reachable. Check connectivity");
    }
  }
  else if(PGPing(ping) == PQPING_REJECT)
  {
    std::cout << "Ping rejeté, essayé plus tard" << std::endl;
    syslog(LOG_INFO, "Rejected ping");
  }
  else if(PGPing(ping) == PQPING_NO_RESPONSE)
  {
    std::cout << "Pas de réponse du ping" << std::endl;
    syslog(LOG_INFO, "No answer from the ping");
  }
  else if(PGPing(ping) == PQPING_NO_ATTEMPT)
  {
    std::cout << "Paramètre de connexion erroné" << std::endl;
    syslog(LOG_INFO, "Incorrect connection seeting");
  }
  else
  {
    std::cout  << "Fonctionne pas :x" << std::endl;
    syslog(LOG_INFO, "Doesn't work.");
  }

  closelog();
  return 0;
}
