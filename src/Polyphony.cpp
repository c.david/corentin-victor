//Librairies
#include "../header/Polyphony.h"
#include <string>
#include <iostream>

//Default constructor
Polyphony::Polyphony() :
  id(1),
  type("Hip-Hop")

{
  std::cout << " " << std::endl;
  std::cout << "Default constructors of the class Polyphony" << std::endl;
  std::cout << "L'id de la polyphony est :  " << id << std::endl;
  std::cout << "Le type de la polyphony est : " << type << std::endl;
}

//Constructor with parameters
Polyphony::Polyphony(unsigned int  _id,
                     std::string _type
                    ) :
  id(_id),
  type(_type)

{
  std::cout << " " << std::endl;
  std::cout << "Constructor with parameters" << std::endl;
  std::cout << "L'id de la polyphony est :  " << id << std::endl;
  std::cout << "Le type de la polyphony est : " << type << std::endl;
}

//Destructive
Polyphony::~Polyphony() {}

// Accessors (getters)
unsigned int Polyphony::getId()
{
  return id;
}

std::string Polyphony::getType()
{
  return type;
}

// Mutators (setters)
void Polyphony::setType(std::string _type)
{
  type = _type;
}
