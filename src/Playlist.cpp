//Librairies
#include <iostream>
#include <string>
#include <vector>
#include "../header/Playlist.h"


//Default constructor
Playlist::Playlist() :
  id(0),
  duration(3600),
  tracks()
{
  std::cout << " " << std::endl;
  std::cout << "Default constructor of class Playlist" << std::endl;
  std::cout << "L'id de la Playlist est : " << id << std::endl;
  std::cout << "La durée de la Playlist est : " << duration << std::endl;
}

//Constructor with parameters
Playlist::Playlist(unsigned int  _id,
                   unsigned int _duration,
                   std::vector<Track> _tracks
                  ):
  id(_id),
  duration(_duration),
  tracks(_tracks)
{
  std::cout << " " << std::endl;
  std::cout << "Constructors with parameters" << std::endl;
  std::cout << "L'id de la Playlist est : " << id << std::endl;
  std::cout << "La durée de la Playlist est : " << duration << " seconds" << std::endl;
}

//Destructive
Playlist::~Playlist() {}

// Methods
void Playlist::writeM3U() {};
void Playlist::writeXSPF() {};

// Accessors (getters)
unsigned int Playlist::getId()
{
  return id;
}

unsigned int Playlist::getDuration()
{
  return duration;
}

std::vector<Track> Playlist::getTrack()
{
  return tracks;
}

// Mutators (setters)
void  Playlist::setDuration(unsigned int _duration)
{
  duration = _duration;
}

void Playlist::setTrack(std::vector<Track> _tracks)
{
  tracks = _tracks;
}

